@tool
extends Node2D


## Drawing with a SubViewport and also the RenderingServer.
## Got help from @efi@chitter.xyz on this one. Thank you Efi!


## Click this toggle to run the code.
@export var tog:bool:
	set(b):
		_run()
		tog=false


func _run():
	# Calls two different attempts to fetch the image
	var _img = await create_image()
	var _img2 = await create_img2()

	$Sprite2D.texture = ImageTexture.create_from_image(_img)
	$Sprite2D2.texture = ImageTexture.create_from_image(_img2)

## Draw stuff with RenderingServer (Does not require any nodes!)
func create_image()->Image:
	var _canvas = RenderingServer.canvas_create()
	var _canvas_item = RenderingServer.canvas_item_create()
	RenderingServer.canvas_item_set_parent(_canvas_item, _canvas)

	var _vp_rid = RenderingServer.viewport_create()
	RenderingServer.viewport_set_size(_vp_rid,200,200)
	RenderingServer.viewport_set_update_mode(_vp_rid, RenderingServer.VIEWPORT_UPDATE_ALWAYS)
	RenderingServer.viewport_attach_canvas(_vp_rid, _canvas)
	RenderingServer.viewport_set_active(_vp_rid, true) # Efi found this method for me!

	## Draw stuff
	RenderingServer.canvas_item_add_rect(_canvas_item, Rect2(0,0,10,10),Color.RED)

	await RenderingServer.frame_post_draw

	var _tex_rid = RenderingServer.viewport_get_texture(_vp_rid)
	var _img = RenderingServer.texture_2d_get(_tex_rid)

	## For each *_create method above, free the rid
	RenderingServer.free_rid(_canvas_item)
	RenderingServer.free_rid(_canvas)
	return _img


## Draw stuff with nodes in the normal way
func create_img2()->Image:
	var _sub_vp = SubViewport.new()
	$".".add_child(_sub_vp) # This is super NB. The subveiwport must be in the tree.
	_sub_vp.size = Vector2(200, 200)
	_sub_vp.render_target_update_mode = SubViewport.UPDATE_ALWAYS

	# Create some test content
	var rect = ColorRect.new()
	_sub_vp.add_child(rect)

	rect.color = Color(1, 0, 0)
	rect.size = Vector2(100, 100)

	await RenderingServer.frame_post_draw

	var _tex = _sub_vp.get_texture()
	var _img = _tex.get_image()
	return _img


